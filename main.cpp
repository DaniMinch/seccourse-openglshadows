#include <stdio.h>
#include "GLee/GLee.h"	//GL header file, including extensions
#include <GL/freeglut.h>
#include "Maths/Maths.h"
#include <QDebug>
#include "imageloader.h"

//Camera & light positions
//VECTOR3D cameraPosition(-2.5f, 3.5f,-2.5f);
//VECTOR3D cameraPosition(4.0f, 5.0f,-3.0f);
VECTOR3D cameraPosition(1.f, 2.0f, 4.0f);
//VECTOR3D lightPosition(2.0f, 3.0f,-2.0f);
VECTOR3D lightPosition(2.0f, 3.0f, 4.0f);

//Size of shadow map
const int shadowMapSize=512;

//Textures
GLuint shadowMapTexture;
GLuint _textureId;

COLOR myColor(1.0f, 1.0f, 1.0f, 1.0f);

//window size
int windowWidth, windowHeight;

//Matrices
MATRIX4X4 lightProjectionMatrix, lightViewMatrix;
MATRIX4X4 cameraProjectionMatrix, cameraViewMatrix;

GLfloat xangle1 = 0;
GLfloat yangle1 = 0;
GLfloat zangle1 = 0;

// текстурные штуки
GLuint loadTexture(Image* image) {
    GLuint textureId; // текстурки характеризуются id
    glGenTextures(1, &textureId); // включаем генерацию текстурных координат
    glBindTexture(GL_TEXTURE_2D, textureId); //Tell OpenGL which texture to edit
    //Map the image to the texture
    glTexImage2D(GL_TEXTURE_2D,           //Always GL_TEXTURE_2D
                 0,                            //0 for now
                 GL_RGB,                       //Format OpenGL uses for image
                 image->width, image->height,  //Width and height
                 0,                            //The border of the image
                 GL_RGB, //GL_RGB, because pixels are stored in RGB format
                 GL_UNSIGNED_BYTE, //GL_UNSIGNED_BYTE, because pixels are stored
                 //as unsigned numbers
                 image->pixels);               //The actual pixel data

    return textureId; //Returns the id of the texture
}

//Called for initiation
bool Init(void)
{
    //Check for necessary extensions
    if(!GLEE_ARB_depth_texture || !GLEE_ARB_shadow)
    {
        printf("I require ARB_depth_texture and ARB_shadow extensionsn\n");
        return false;
    }

    //Load identity modelview
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    //Shading states
    glShadeModel(GL_SMOOTH);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

    //Depth states
    //glClearDepth(1.0f);
    glDepthFunc(GL_LEQUAL);
    glEnable(GL_DEPTH_TEST);

    glEnable(GL_CULL_FACE);

    //We use glScale when drawing the scene
    glEnable(GL_NORMALIZE);

    //Create the shadow map texture
    glGenTextures(1, &shadowMapTexture);
    glBindTexture(GL_TEXTURE_2D, shadowMapTexture);
    glTexImage2D(	GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, shadowMapSize, shadowMapSize, 0,
                    GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

    //Use the color as the ambient and diffuse material
    glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
    glEnable(GL_COLOR_MATERIAL);

    //White specular material color, shininess 16
    glMaterialfv(GL_FRONT, GL_SPECULAR, myColor);
    glMaterialf(GL_FRONT, GL_SHININESS, 16.0f);

    //Image* image = loadBMP("pict.bmp");
    //_textureId = loadTexture(image);

    return true;
}

void DrawScene()
{
    //Display lists for objects
    static GLuint roomList=0, spheresList=0, torusList=0;

    if (!roomList)
    {
        roomList = glGenLists(1);
        float x,y,z;
        glNewList(roomList, GL_COMPILE);
        {
            glColor3f(1,1,1);
            glBegin(GL_QUADS);									// Begin Drawing Quads
            {
                // Floor
                glNormal3f(0.0f, 1.0f, 0.0f);
                y = 0.0;// Normal Pointing Up
                for (x = -2.0f; x < 2.0f; x += 0.05f)
                {
                    for (z = -3.0f; z < 3.0f; z += 0.05f)
                    {
                        glVertex3f(x, y, z);
                        glVertex3f(x, y, z +0.05f);
                        glVertex3f(x + 0.05f, y, z + 0.05f);
                        glVertex3f(x + 0.05f, y, z);
                    }
                }
                // Ceiling
                glNormal3f(0.0f,-1.0f, 0.0f);					// Normal Point Down
                y = 3.0f;// Normal Pointing Up
                for (x = -2.0f; x < 2.0f; x += 0.05f)
                {
                    for (z = -3.0f; z < 3.0f; z += 0.05f)
                    {
                        glVertex3f(x, y, z);
                        glVertex3f(x + 0.05f, y, z);
                        glVertex3f(x + 0.05f, y, z + 0.05f);
                        glVertex3f(x, y, z + 0.05f);
                    }
                }
                // Front Wall
//                glNormal3f(0.0f, 0.0f, 1.0f);					// Normal Pointing Away From Viewer
//                z = 3.0f;
//                for (x = -2.0f; x < 2.0f; x += 0.05f)
//                {
//                    for (y = -3.0f; y < 3.0f; y += 0.05f)
//                    {
//                        glVertex3f(x, y, z);
//                        glVertex3f(x, y + 0.05f, z);
//                        glVertex3f(x + 0.05f, y + 0.05f, z);
//                        glVertex3f(x + 0.05f, y, z);
//                    }
//                }
                // Back Wall
                glNormal3f(0.0f, 0.0f,1.0f);					// Normal Pointing Towards Viewer
                z = -3.0f;
                for (x = -2.0f; x < 2.0f; x += 0.05f)
                {
                    for (y = 0.0f; y < 3.0f; y += 0.05f)
                    {
                        glVertex3f(x, y, z);
                        glVertex3f(x + 0.05f, y, z);
                        glVertex3f(x + 0.05f, y + 0.05f, z);
                        glVertex3f(x, y + 0.05f, z);
                    }
                }
                // Left Wall
                glNormal3f(1.0f, 0.0f, 0.0f);					// Normal Pointing Right
                x = -2.0f;
                for (y = 0.0f; y < 3.0f; y += 0.05)
                {
                    for (z = -3.0f; z < 3.0f; z += 0.05)
                    {
                        glVertex3f(x, y, z);
                        glVertex3f(x, y + 0.05f, z);
                        glVertex3f(x, y + 0.05f, z + 0.05f);
                        glVertex3f(x, y, z + 0.05f);
                    }
                }
//                // Right Wall
                glNormal3f(-1.0f, 0.0f, 0.0f);					// Normal Pointing Left
                x = 2.0f;
                for (y = 0.0f; y < 3.0f; y += 0.05f)
                {
                    for (z = -3.0f; z < 3.0f; z += 0.05f)
                    {
                        glVertex3f(x, y, z);
                        glVertex3f(x, y, z + 0.05f);
                        glVertex3f(x, y + 0.05f, z + 0.05f);
                        glVertex3f(x, y + 0.05f, z);
                    }
                }
            }
            glEnd();
        }
        glEndList();
    }
    //Create spheres list if necessary
    if(!spheresList)
    {
        spheresList=glGenLists(1);
        glNewList(spheresList, GL_COMPILE);
        {

            glColor4f(0.0f, 1.0f, 1.0f, 0.1f);

            glEnable(GL_BLEND);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

            glPushMatrix();

            glTranslatef(0.45f, 1.0f, 0.45f);
            glPushMatrix();
            glRotated(-90, 1, 0, 0);
            glutSolidCone(0.2, 0.3, 24, 24);
            glPopMatrix();

            glTranslatef(-0.9f, 0.0f, 0.0f);
            glPushMatrix();
            glRotated(-90, 1, 0, 0);
            glutSolidCone(0.2, 0.3, 24, 24);
            glPopMatrix();

            glTranslatef(0.0f, 0.0f,-0.9f);
            glPushMatrix();
            glRotated(-90, 1, 0, 0);
            glutSolidCone(0.2, 0.3, 24, 24);
            glPopMatrix();

            glTranslatef(0.9f, 0.0f, 0.0f);
            glPushMatrix();
            glRotated(-90, 1, 0, 0);
            glutSolidCone(0.2, 0.3, 24, 24);
            glPopMatrix();

            glPopMatrix();
            glDisable(GL_BLEND);
        }
        glEndList();
    }

    //Create torus if necessary
    if(!torusList)
    {
        torusList=glGenLists(1);
        glNewList(torusList, GL_COMPILE);
        {
            glDisable(GL_COLOR_MATERIAL);
            glPushMatrix();

            glTranslatef(0.0f, 0.5f, 0.0f);
            glRotatef(90.0f, 1.0f, 0.0f, 0.0f);

            float diffuse_torus[4] = {1.0f, 1.0f, .0f, 1.0f};
            float ambient_torus[4] = {0.2f, 0.2f, 0.2f, 1.0f};
            float specular_torus[4] = {0.3f, 0.4f, 0.2f, 1.0f};
            float shininess_torus [1] = { 1.f };
            glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse_torus); // цвет шарика
            glMaterialfv(GL_FRONT, GL_AMBIENT, ambient_torus);
            glMaterialfv(GL_FRONT, GL_SPECULAR, specular_torus); // отраженный свет
            glMaterialfv(GL_FRONT, GL_SHININESS, shininess_torus); // степень отраженного света

            glutSolidTorus(0.2, 0.5, 24, 48);

            glPopMatrix();

            glPushMatrix();
            float shininess_sphere [1] = { 128.f };
            glMaterialfv(GL_FRONT, GL_SHININESS, shininess_sphere); // степень отраженного света
            glTranslatef(-0.2f, 0.5f, 0.8f);
            glutSolidSphere(0.2, 32, 32);
            glPopMatrix();
            glEnable(GL_COLOR_MATERIAL);
        }
        glEndList();
    }


    //Draw objects
    glCallList(roomList);
    glCallList(torusList);
    glCallList(spheresList);
}

//Called to draw scene
void Display(void)
{
    //Calculate & save matrices
    glPushMatrix();

    glLoadIdentity();
    gluPerspective(45.0f, (float)windowWidth/windowHeight, 1.0f, 100.0f);
    glGetFloatv(GL_MODELVIEW_MATRIX, cameraProjectionMatrix);

    glLoadIdentity();
    gluLookAt(cameraPosition.x, cameraPosition.y, cameraPosition.z,
                0.0f, 0.0f, 0.0f,
                0.0f, 1.0f, 0.0f);
    glGetFloatv(GL_MODELVIEW_MATRIX, cameraViewMatrix);

    glLoadIdentity();
    gluPerspective(45.0f, 1.0f, 2.0f, 8.0f);
    glGetFloatv(GL_MODELVIEW_MATRIX, lightProjectionMatrix);

    glLoadIdentity();
    gluLookAt(	lightPosition.x, lightPosition.y, lightPosition.z,
                0.0f, 0.0f, 0.0f,
                0.0f, 1.0f, 0.0f);
    glGetFloatv(GL_MODELVIEW_MATRIX, lightViewMatrix);

    glPopMatrix();

    //First pass - from light's point of view
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_PROJECTION);
    glLoadMatrixf(lightProjectionMatrix);

    glMatrixMode(GL_MODELVIEW);
    glLoadMatrixf(lightViewMatrix);

    //Use viewport the same size as the shadow map
    glViewport(0, 0, shadowMapSize, shadowMapSize);

    //Draw back faces into the shadow map
    glCullFace(GL_FRONT);

    //Disable color writes, and use flat shading for speed
    glShadeModel(GL_FLAT);
    glColorMask(0, 0, 0, 0);

    //Draw the scene
    DrawScene();

    //Read the depth buffer into the shadow map texture
    glBindTexture(GL_TEXTURE_2D, shadowMapTexture);
    glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 0, 0, shadowMapSize, shadowMapSize);

    //restore states
    glCullFace(GL_BACK);
    glShadeModel(GL_SMOOTH);
    glColorMask(1, 1, 1, 1);

    //2nd pass - Draw from camera's point of view
    glClear(GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_PROJECTION);
    glLoadMatrixf(cameraProjectionMatrix);

    glMatrixMode(GL_MODELVIEW);
    glLoadMatrixf(cameraViewMatrix);

    glViewport(0, 0, windowWidth, windowHeight);

    //Use dim light to represent shadowed areas
    glLightfv(GL_LIGHT1, GL_POSITION, new float [4]{lightPosition.x, lightPosition.y, lightPosition.z, 1.f});
    glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, new float[3]{-lightPosition.x, -lightPosition.y,-lightPosition.z});
//    glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, new float[3]{0.0f, -2.0f,0.0f});
    glLightf(GL_LIGHT1, GL_SPOT_CUTOFF, 30);
    glLightfv(GL_LIGHT1, GL_AMBIENT, myColor*0.2f);
    glLightfv(GL_LIGHT1, GL_DIFFUSE, myColor*0.2f);
    glLightfv(GL_LIGHT1, GL_SPECULAR, black);
    glEnable(GL_LIGHT1);
    glEnable(GL_LIGHTING);

    DrawScene();



    //3rd pass
    //Draw with bright light
    glLightfv(GL_LIGHT1, GL_DIFFUSE, myColor);
    glLightfv(GL_LIGHT1, GL_SPECULAR, myColor);

    //Calculate texture matrix for projection
    //This matrix takes us from eye space to the light's clip space
    //It is postmultiplied by the inverse of the current view matrix when specifying texgen
    static MATRIX4X4 biasMatrix(0.5f, 0.0f, 0.0f, 0.0f,
                                0.0f, 0.5f, 0.0f, 0.0f,
                                0.0f, 0.0f, 0.5f, 0.0f,
                                0.5f, 0.5f, 0.5f, 1.0f);	//bias from [-1, 1] to [0, 1]
    MATRIX4X4 textureMatrix=biasMatrix*lightProjectionMatrix*lightViewMatrix;

    //Set up texture coordinate generation.
    glTexGeni(GL_S, GL_TEXTURE_GEN_MODE, GL_EYE_LINEAR);
    glTexGenfv(GL_S, GL_EYE_PLANE, textureMatrix.GetRow(0));
    glEnable(GL_TEXTURE_GEN_S);

    glTexGeni(GL_T, GL_TEXTURE_GEN_MODE, GL_EYE_LINEAR);
    glTexGenfv(GL_T, GL_EYE_PLANE, textureMatrix.GetRow(1));
    glEnable(GL_TEXTURE_GEN_T);

    glTexGeni(GL_R, GL_TEXTURE_GEN_MODE, GL_EYE_LINEAR);
    glTexGenfv(GL_R, GL_EYE_PLANE, textureMatrix.GetRow(2));
    glEnable(GL_TEXTURE_GEN_R);

    glTexGeni(GL_Q, GL_TEXTURE_GEN_MODE, GL_EYE_LINEAR);
    glTexGenfv(GL_Q, GL_EYE_PLANE, textureMatrix.GetRow(3));
    glEnable(GL_TEXTURE_GEN_Q);

    //Bind & enable shadow map texture
    glBindTexture(GL_TEXTURE_2D, shadowMapTexture);
    glEnable(GL_TEXTURE_2D);

    //Enable shadow comparison
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE_ARB, GL_COMPARE_R_TO_TEXTURE);

    //Shadow comparison should be true (ie not in shadow) if r<=texture
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC_ARB, GL_LEQUAL);

    //Shadow comparison should generate an INTENSITY result
    glTexParameteri(GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE_ARB, GL_INTENSITY);

    //Set alpha test to discard false comparisons
    glAlphaFunc(GL_GEQUAL, 0.99f);
    glEnable(GL_ALPHA_TEST);

    DrawScene();

    //Disable textures and texgen
    glDisable(GL_TEXTURE_2D);

    glDisable(GL_TEXTURE_GEN_S);
    glDisable(GL_TEXTURE_GEN_T);
    glDisable(GL_TEXTURE_GEN_R);
    glDisable(GL_TEXTURE_GEN_Q);

    //Restore other states
    glDisable(GL_LIGHTING);
    glDisable(GL_ALPHA_TEST);

    // шарик с камерой
    glPushMatrix();
    glColor4f(0.7f, 0.4f, 0.0f, 1.0f);
    glTranslatef(lightPosition.x, lightPosition.y, lightPosition.z);																			// Notice We're Still In Local Coordinate System
    glutSolidSphere(0.02, 8, 4);
    glPopMatrix();



    //Set matrices for ortho
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    gluOrtho2D(-1.0f, 1.0f, -1.0f, 1.0f);

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();

    //reset matrices
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();

    glFinish();
    glutSwapBuffers();
}

//Called on window resize
void Reshape(int w, int h)
{
    //Save new window size
    windowWidth=w, windowHeight=h;

    //Update the camera's projection matrix
    glPushMatrix();
    glLoadIdentity();
    gluPerspective(45.0f, (float)windowWidth/windowHeight, 1.0f, 100.0f);
    glGetFloatv(GL_MODELVIEW_MATRIX, cameraProjectionMatrix);
    glPopMatrix();
}

//Called when a key is pressed
void Keyboard(unsigned char key, int x, int y)
{
    switch (key) {
    case 'q': myColor.r -= 0.05; break;
    case 'w': myColor.r += 0.05;  break;
    case 'a': myColor.g -= 0.05;  break;
    case 's': myColor.g += 0.05;  break;
    case 'z': myColor.b -= 0.05; break;
    case 'x': myColor.b += 0.05; break;
    case 27: exit(0);  break; //If escape is pressed, exit
    default: return;
    }
    glutPostRedisplay();
}

void Special_Keys (int key, int x, int y)
{

    switch (key) {
    case GLUT_KEY_LEFT :  lightPosition.x -= 0.02f;  break;
    case GLUT_KEY_RIGHT:  lightPosition.x += 0.02f;  break;
    case GLUT_KEY_UP   :  lightPosition.y += 0.02f;  break;
    case GLUT_KEY_DOWN :  lightPosition.y -= 0.02f;  break;
    case GLUT_KEY_PAGE_DOWN: lightPosition.z += 0.02f; break;
    case GLUT_KEY_PAGE_UP: lightPosition.z -= 0.02f; break;
    default: return;
    }
    glutPostRedisplay();
}

int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(640, 512);
    glutCreateWindow("Shadow Mapping");

    if(!Init())
        return 0;

    glutDisplayFunc(Display);
    glutReshapeFunc(Reshape);
    glutKeyboardFunc(Keyboard);
    glutSpecialFunc(Special_Keys);
    glutMainLoop();
    return 0;
}
