QT       += opengl

CONFIG	 += C++14

QMAKE_LFLAGS_RELEASE += -static -static-libgcc

TARGET = OpenGLShadows
TEMPLATE = app

LIBS += libfreeglut -lopengl32 -lglu32

HEADERS += \
    Maths/COLOR.h \
    Maths/Maths.h \
    Maths/MATRIX4X4.h \
    Maths/PLANE.h \
    Maths/VECTOR2D.h \
    Maths/VECTOR3D.h \
    Maths/VECTOR4D.h \
    GLee/GLee.h \
    Extensions/ARB_multitexture_extension.h \
    Extensions/EXT_texture_env_combine_extension.h \
    imageloader.h

SOURCES += \
    main.cpp \
    Maths/COLOR.cpp \
    Maths/MATRIX4X4.cpp \
    Maths/PLANE.cpp \
    Maths/VECTOR2D.cpp \
    Maths/VECTOR3D.cpp \
    Maths/VECTOR4D.cpp \
    GLee/GLee.c \
    Extensions/ARB_multitexture_extension.cpp \
    Extensions/EXT_texture_env_combine_extension.cpp \
    imageloader.cpp

DISTFILES += \
    GLee/extensionList.txt \
    GLee/readme.txt
